"""Fulfil target sink class, which handles writing streams."""


from datetime import datetime, timedelta

import requests
from singer_sdk.sinks import RecordSink


class FulfilSink(RecordSink):
    """Fulfil target sink class."""

    @property
    def base_url(self):
        return f"https://{self.config.get('instance')}.fulfil.io/api/v2/model"

    def get_headers(self):
        headers = {}
        headers["X-API-KEY"] = self.config.get("api_key")
        headers["Content-Type"] = "application/json"
        return headers

    def process_purchase_orders(self, record):

        product_ids = []
        for line_item in record["lineItems"]:
            product_ids.append(
                {
                    "product": line_item["productOptionId"],
                    "quantity": line_item["qty"],
                    "unit_price": str(line_item["unitPrice"]),
                    "unit": 1,
                }
            )

        lines = []
        lines.append(["create", product_ids])

        # Order and shipping date
        order_date = record["purchase_date"]
        order_date_p = datetime.strptime(order_date, "%Y-%m-%d")
        requested_delivery_date = order_date_p + timedelta(days=7)
        requested_delivery_date = requested_delivery_date.strftime("%Y-%m-%d")

        data = [
            {
                "reference": record["reference"],
                "description": "Test Create PO API Inventoro",
                "number": record["number"],
                "lines": lines,
                "party": record["supplierId"],
                "warehouse": record["warehouseId"],
                "requested_shipping_date": order_date,
                "requested_delivery_date": requested_delivery_date,
                "shipment_method": "manual",
            }
        ]
        # new_record_ids = model.create(data)
        new_record_ids = requests.post(
            f"{self.base_url}/purchase.purchase", headers=self.get_headers(), json=data
        )
        print(new_record_ids.text)

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""

        if self.stream_name == "purchase_orders":
            self.process_purchase_orders(record)
