"""Fulfil target class."""

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_fulfil.sinks import FulfilSink


class TargetFulfil(Target):
    """Sample target for Fulfil."""

    name = "target-fulfil"
    config_jsonschema = th.PropertiesList(
        th.Property("api_key", th.StringType, required=True),
        th.Property("instance", th.StringType, required=True),
    ).to_dict()
    default_sink_class = FulfilSink


if __name__ == "__main__":
    TargetFulfil.cli()
